<?php

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Trystar\Config\Config;
use Sda\Trystar\Controller\MainController;
use Sda\Trystar\Phase\PhaseRepository;
use Sda\Trystar\Request\Request;
use Sda\Trystar\Response\Response;

require_once __DIR__ . '/../vendor/autoload.php';

$config = new Configuration();
$request = new Request();
$response = new Response();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$repo =new PhaseRepository($dbh);
$app = new MainController($repo, $request, $response);
$app->run();