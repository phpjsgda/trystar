<?php

namespace Sda\Trystar\Response;

use Sda\Trystar\Light\LightCollection;

class Response {
    
    const CROSSROAD_URL = 'http://crossroad.local/api/lights';
    const STATUS_OK = 200;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_PERMISION_DENIED = 403;
    const STATUS_NOT_FOUND = 404;

    public function sendToCrossroad(LightCollection $lightCollection)
    {
        $jsonData = json_encode($lightCollection);
        var_dump($jsonData);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::CROSSROAD_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 1,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            die('Problem z wysylka');
        }
    }

    public function send($dataToSend, $statusCode = self::STATUS_OK){
        
        http_response_code($statusCode);

        $data = [
            'errors' => [],
            'data' => $dataToSend
        ];
        
        echo json_encode($data);
        exit();
    }
}