<?php

namespace Sda\Trystar\Light;

class LightFactory {

    public static function makeFromRepository(array $row)
    {
        $builder = new LightBuilder();

            return  $builder
                ->withId((int)$row['id'])
                ->withCrossroadId((int)$row['crossroads_id'])
                ->withExternalLightId((int)$row['external_light_id'])
                ->withState($row['state'])
                ->build();
    }
}
