<?php

namespace Sda\Trystar\Light;

use Doctrine\DBAL\Connection;
use Sda\Trystar\Phase\Phase;

class LightRepository {
    
    private $dbh;
    
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }
    
    public function getLightFromPhase(Phase $phase)
    {
        $sth = $this->dbh->prepare(
                'SELECT l.*, ls.state FROM `lights` AS l '
                . 'JOIN `lights_states` AS ls ON ls.`lights_id` = l.`id`'
                . 'WHERE l.`crossroads_id` = :crossroad_id AND ls.`phases_id` = :phases_id'
        );
        $sth->bindValue('crossroad_id', $phase->getCrossroadId(), \PDO::PARAM_INT);
        $sth->bindValue('phases_id', $phase->getId(), \PDO::PARAM_INT);
        $sth->execute();

        $lightsData = $sth->fetchAll();
        
        $lights = new LightCollection();
        foreach($lightsData as $row){

            $light = LightFactory::makeFromRepository($row);       
            $lights->add($light);
        }
        return $lights;
         
        var_dump($lights);
    }
}
