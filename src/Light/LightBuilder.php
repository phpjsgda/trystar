<?php

namespace Sda\Trystar\Light;

class LightBuilder {

    /**
     * @var int
     */
    private $id;
    
    private $crossroadId;
    
    private $externalLightId;
    /**
     * @var string
     */
    private $state = 'suspended';

    /**
     * @return Light
     */
    public function build()
    {
        return new Light(
            $this->id,
            $this->crossroadId,
            $this->externalLightId,
            $this->state
        );
    }

    /**
     * @param int $id
     * @return LightBuilder
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    public function withCrossroadId($crossroadId)
    {
        $this->crossroadId = $crossroadId;
        return $this;
    }
    
    public function withExternalLightId($externalLightId)
    {
        $this->externalLightId = $externalLightId;
        return $this;
    }

    /**
     * @param string $state
     * @return LightBuilder
     */
    public function withState($state)
    {
        $this->state = $state;
        return $this;
    }
}
