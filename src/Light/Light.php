<?php

namespace Sda\Trystar\Light;

class Light implements \JsonSerializable {
    
    private $id;
    private $crossroadsId;
    private $externalLightId;
    private $state;
    
    public function __construct($id, $crossroadsId, $externalLightId, $state) {
        $this->id = $id;
        $this->crossroadsId = $crossroadsId;
        $this->externalLightId = $externalLightId;
        $this->state = $state;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getCrossroadsId()
    {
        return $this->crossroadsId;
    }
    
    public function getExternalLightId()
    {
        return $this->externalLightId;
    }
    
    public function getState()
    {
        return $this->state;
    }
    
    public function jsonSerialize()
    {
        return [
            'id' => $this->externalLightId,
            'state' => $this->state
        ];
    }
}
