<?php

namespace Sda\Trystar\Phase;

class PhaseFactory {

    public static function makeFromRepository(array $phaseData)
    {
        return new Phase(
                $phaseData['id'],
                $phaseData['delay'],
                $phaseData['phase_order'],
                $phaseData['actual'],
                $phaseData['crossroads_id']
                );
    }
}
