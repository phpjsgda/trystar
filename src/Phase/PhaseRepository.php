<?php

namespace Sda\Trystar\Phase;

use Doctrine\DBAL\Connection;

class PhaseRepository {

    private $dbh;
    
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }
    
    public function getPhaseByCrossroadId($crossroadId)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `phases` WHERE `crossroads_id` = :id AND `actual` = \'yes\'');
        $sth->bindValue('id', $crossroadId, \PDO::PARAM_INT);
        $sth->execute();

        $phaseData = $sth->fetch();
        
        if (false === $phaseData) {
           throw new PhaseNotFoundException();
        }
        return PhaseFactory::makeFromRepository($phaseData);
    }
    
    public function getPhaseByCrossroadIdAndPhaseOrder($crossroadId, $phaseOrder)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `phases` WHERE `crossroads_id` = :id AND `phase_order` = :nazwa');
        $sth->bindValue('id', $crossroadId, \PDO::PARAM_INT);
        $sth->bindValue('nazwa', $phaseOrder, \PDO::PARAM_INT);
        $sth->execute();

        $phaseData = $sth->fetch();
        
        if (false === $phaseData) {
           throw new PhaseNotFoundException();
        }
        return PhaseFactory::makeFromRepository($phaseData);
    }
    
    public function getAmountOfPhases($crossroadId)
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `phases` WHERE `crossroads_id` = :id');
        $sth->bindValue('id', $crossroadId, \PDO::PARAM_INT);
        $sth->execute();
        
        $phaseData = $sth->fetchColumn();
        
        return $phaseData;
    }
    
    public function changePhaseId(Phase $actualPhase, Phase $nextPhase)
    {
        $this->dbh->update('phases', ['actual'=>'no'], ['id'=>$actualPhase->getId()]);
        $this->dbh->update('phases', ['actual'=>'yes'], ['id'=>$nextPhase->getId()]);
    }
    
    public function addTimeLog(Phase $phase)
    {
        $timeNow = date('Y-m-d H:i:s', time());
        $this->dbh->insert('lightsdata', ['phase_id'=>$phase->getId(), 'time'=>$timeNow]);
    }
    
    public function compareTime(Phase $actualPhase)
    {
        $sth = $this->dbh->prepare('SELECT `time` FROM `lightsdata` ORDER BY `time` DESC LIMIT 1');
        $sth->execute();
        $changeTimeData = strtotime($sth->fetchColumn());
        $timeNow = time();
        
        var_dump($timeNow - $changeTimeData, (int)$actualPhase->getDelay());
        
        return $timeNow - $changeTimeData > (int)$actualPhase->getDelay();
    }
}
