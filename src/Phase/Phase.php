<?php

namespace Sda\Trystar\Phase;

class Phase {

    private $id;
    private $delay;
    private $phaseOrder;
    private $actual;
    private $crossroadsId;
    
    public function __construct($id, $delay, $phaseOrder, $actual, $crossroadsId) {
        $this->id = $id;
        $this->delay = $delay;
        $this->phaseOrder = $phaseOrder;
        $this->actual = $actual;
        $this->crossroadsId = $crossroadsId;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getDelay()
    {
        return $this->delay;
    }
    
    public function getPhaseOrder()
    {
        return $this->phaseOrder;
    }
    
    public function getActual()
    {
        return $this->actual;
    }
    
    public function getCrossroadId()
    {
        return $this->crossroadsId;
    }
}
