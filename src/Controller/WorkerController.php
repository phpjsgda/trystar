<?php

namespace Sda\Trystar\Controller;

use Sda\Trystar\Phase\PhaseRepository;
use Sda\Trystar\Request\Request;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Phase\PhaseNotFoundException;
use Sda\Trystar\Light\LightRepository;

class WorkerController {
    
    private $phaseRepository;
    private $request;
    private $response;
    private $lightRepository;
    
    public function __construct(
        PhaseRepository $phaseRepository,
        LightRepository $lightRepository,
        Request $request,
        Response $response
        ) 
        
        {
        $this->phaseRepository = $phaseRepository;
        $this->lightRepository = $lightRepository;
        $this->request = $request;
        $this->response = $response;
    }

    public function run()
    {
        $crossroadId = 1;
        
        try {
            $actualPhase = $this->phaseRepository->getPhaseByCrossroadId($crossroadId);
            $phaseOrder = $actualPhase->getPhaseOrder()+1;
            if($phaseOrder > $this->phaseRepository->getAmountOfPhases($crossroadId)) {
                $phaseOrder = 1;
            }
            $nextPhase = $this->phaseRepository->getPhaseByCrossroadIdAndPhaseOrder($crossroadId, $phaseOrder);
            
            if(true === $this->phaseRepository->compareTime($actualPhase)){
                $lights = $this->lightRepository->getLightFromPhase($nextPhase);
                $this->response->sendToCrossroad($lights);
                $this->phaseRepository->changePhaseId($actualPhase, $nextPhase);
                $this->phaseRepository->addTimeLog($nextPhase);
            }
            
        } catch (PhaseNotFoundException $e){
            $this->response->send('Nieodnaleziono obiektu Phase o ID: ' . $crossroadId, Response::STATUS_NOT_FOUND);
        }
    }
}
