<?php

use Sda\Trystar\Controller\WorkerController;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Trystar\Config\Config;
use Sda\Trystar\Phase\PhaseRepository;
use Sda\Trystar\Request\Request;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Light\LightRepository;

require_once __DIR__ . '/../vendor/autoload.php';

$config = new Configuration();
$request = new Request();
$response = new Response();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$repoPhase = new PhaseRepository($dbh);
$repoLight = new LightRepository($dbh);
$worker = new WorkerController($repoPhase, $repoLight, $request, $response);
$worker->run();
