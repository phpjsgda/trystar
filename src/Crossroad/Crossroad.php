<?php

namespace Sda\Trystar\Crossroad;

class Crossroad {

    private $id;
    private $name;
    private $securityKey;
    
    public function __construct($id, $name, $securityKey) {
        $this->id = $id;
        $this->name = $name;
        $this->securityKey = $securityKey;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getSecurityKey()
    {
        return $this->securityKey;
    }
}
