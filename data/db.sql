DROP TABLE IF EXISTS `lights_states`;
DROP TABLE IF EXISTS `phases`;
DROP TABLE IF EXISTS `lights`;
DROP TABLE IF EXISTS `crossroads`;


CREATE TABLE `crossroads` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `security_key` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `lights` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `crossroads_id` int(6) NOT NULL,
  `external_light_id` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `crossroads_id` (`crossroads_id`),
  CONSTRAINT `lights_ibfk_1` FOREIGN KEY (`crossroads_id`) REFERENCES `crossroads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `phases` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `crossroads_id` int(6) NOT NULL,
  `delay` mediumint(8) unsigned NOT NULL,
  `phase_order` tinyint(3) unsigned NOT NULL,
  `actual` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  CONSTRAINT `phases_ibfk_1` FOREIGN KEY (`crossroads_id`) REFERENCES `crossroads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `lights_states` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `lights_id` int(6) NOT NULL,
  `phases_id` int(6) NOT NULL,
  `state` enum('shutdown','suspended','red_light','green_light','yellow_light','red_yellow_light') NOT NULL DEFAULT 'suspended',
  PRIMARY KEY (`id`),  
KEY `lights_id` (`lights_id`),
  KEY `phases_id` (`phases_id`),
  CONSTRAINT `lights_states_ibfk_1` FOREIGN KEY (`lights_id`) REFERENCES `lights` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lights_states_ibfk_2` FOREIGN KEY (`phases_id`) REFERENCES `phases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `crossroads` (`id`, `name`, `security_key`)
VALUES
	(1,'Grunwaldzka','KJHjhLGLhgLGlhgLHG'),
	(2,'Dworzec glowny','JHiUYFUYFtydIYTDYdyd');

INSERT INTO `lights` (`id`, `crossroads_id`, `external_light_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,1,3),
	(4,1,4),
	(5,2,1),
	(6,2,2),
	(7,2,3),
	(8,2,4);

INSERT INTO `phases` (`id`, `crossroads_id`, `delay`, `phase_order`, `actual`)
VALUES
	(1,1,60,1,'yes'),
	(2,1,10,2,'no'),
	(3,1,60,3,'no'),
	(4,1,10,4,'no'),
	(5,2,100,1,'yes'),
	(6,2,20,2,'no'),
	(7,2,100,3,'no'),
	(8,2,20,4,'no');

INSERT INTO `lights_states` (`lights_id`, `phases_id`, `state`)
VALUES
	(1,1,'green_light'),
	(2,1,'green_light'),
	(3,1,'red_light'),
	(4,1,'red_light'),
	(1,2,'yellow_light'),
	(2,2,'yellow_light'),
	(3,2,'red_yellow_light'),
	(4,2,'red_yellow_light'),
	(1,3,'red_light'),
	(2,3,'red_light'),
	(3,3,'green_light'),
	(4,3,'green_light'),
	(1,4,'red_yellow_light'),
	(2,4,'red_yellow_light'),
	(3,4,'yellow_light'),
	(4,4,'yellow_light');